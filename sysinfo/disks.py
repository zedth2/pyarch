#!/usr/bin/python3
'''
Author : Zachary Harvey






'''

from os.path import exists
from glob import glob
from sysutils import helpers

SYSFS_BLOCK_PATH = '/sys/block/{disk}'
DEV_PATH = '/dev/{disk}'
MTAB = '/etc/mtab'
FSTAB = '/etc/fstab'


class NoPartition(Exception):
    pass


class Disk:
    def __init__(self, device, loadparts=False):
        if exists(DEV_PATH.format(disk=device)) and \
        exists(SYSFS_BLOCK_PATH.format(disk=device)):
            self.__disk = device
        else:
            raise ValueError('Invalid disk {} : SYSFS {} and device {} does not exist'.format(device, SYSFS_BLOCK_PATH.format(disk=device), DEV_PATH.format(disk=device)))
        self.parts = []
        if loadparts:
            pass

    def load_minor(self, minor):
        p = Partition(minor, self.disk)
        self.parts.append(p)
        return p

    def find_mount(self, mntpnt):
        part = None
        for p in self.parts:
            if p.mount is not None and p.mount == mntpnt:
                part = p
                break
        return part

    @property
    def disk(self):
        return self.__disk

    @property
    def logicalBlockSize(self):
        '''
        Block Size I believe is always in bytes.
        '''
        path = (SYSFS_BLOCK_PATH + '/device/block/{disk}/queue/logical_block_size').format(disk=self.disk)
        size = open(path).readlines()
        return int(size[0].strip())

    @property
    def totalSize(self):
        '''
        Returns the disk size in bytes.
        '''
        path = (SYSFS_BLOCK_PATH + '/device/block/{disk}/size').format(disk=self.disk)
        size = open(path).readlines()
        return int(size[0].strip()) * self.logicalBlockSize

    @property
    def getDevPath(self):
        return DEV_PATH.format(disk=self.disk)

    @property
    def sysfs_block(self):
        return SYSFS_BLOCK_PATH.format(disk=self.disk)

    @property
    def getPartitions(self):
        pass

    def all_minors(self):
        mins = []
        for m in glob(self.sysfs_block+'/'+self.disk+'[0-9]*'):
            cnt = len(m)-1
            cur = ''
            while cnt > 0:
                if m[cnt].isdigit():
                    cur = cur + m[cnt]
                else:
                    break
                cnt -= 1
            mins.append(cur)
        return mins


class Partition:
    def __init__(self, minor, parentdev):
        self.minor = minor
        self.parent = parentdev
        self.full = DEV_PATH.format(disk=self.parent+str(self.minor))
        self.mount = None
        self.type = None
        self.options = None
        self.dump = None
        self.fsck = None
        if not exists(DEV_PATH.format(disk=self.parent+str(self.minor))):
            raise NoPartition('Invalid partition {} : device {} does not exist'.format(self.parent+str(self.minor), DEV_PATH.format(disk=self.parent+str(self.minor))))

    def read_mtab(self):
        ops = find_mtab_line(self.full)
        if ops == '':
            return False
        ops = ops.split()
        self.mount = ops[1]
        self.type = ops[2]
        self.options = ops[3]
        self.dump = ops[4]
        self.fsck = ops[5]
        return True

    def ismounted(self):
        return find_mtab_line(self.full) != ''

    def infstab(self, fstab=FSTAB):
        return find_fstab_line(self.full, fstab) != ''

def find_mtab_line(fulldevice):
    return _find_dev_line(fulldevice)

def find_fstab_line(fulldevice, fstab=FSTAB):
    return _find_dev_line(fulldevice, fstab)

def _find_dev_line(fulldevice, readfrom=MTAB):
    line = ''
    with open(readfrom) as tab:
        for l in tab.readlines():
            if l.startswith(fulldevice):
                line = l
                break
    return line

def load_all_mounts():
    disks = {}
    with open(MTAB) as tab:
        for l in tab.readlines():
            splits = l.split()
            #Match on sd* because I don't feel like fixing matches for loop0 and sr0
            if splits[0].startswith('/dev/sd'):
                helpers.debug('Loading device {}'.format(splits[0]))
                dev = splits[0][5:8]
                minor = splits[0][8]
                if not dev in disks:
                    disks[dev] = Disk(dev)
                p = disks[dev].load_minor(minor)
                p.read_mtab()
    return tuple(disks.values())

def find_mount(dirmnt):
    part = None
    for d in load_all_mounts():
        p = d.find_mount(dirmnt)
        if not p is None:
            part = p
            break
    return part

def find_all_disks():
    pass


if __name__ == '__main__':
    d = load_all_mounts()
    print(d[0].all_minors())
