#!/usr/bin/python3
'''
Author : Zachary Harvey






'''

from sysutils import SystemSetup
from sysutils.diskutils import FileSystem

from sysutils.helpers import ExecutionError

def tester():
    x = [{
            'device': 'sda',
            'label' : 'gpt',
            'partition': [ {'fs':'fat32',
                            'size' : '100MB',
                            'name' : 'BOOT',
                            'flag' : 'boot',
                            'type' : 'primary',
                            'mount': '/boot',
                            },
                            {'fs':'ext3',
                            'size' : '-1s',
                            'name' : 'ROOT',
                            'type' : 'primary',
                            'mount': '/',
                            }
                          ]
        },{"device" : "sdb",
        "partition" : [
                    {
                "fs" : "ntfs",
                "disk" : "sdb1",
                "mount" : "/run/media/tux/expand"
                    }]}]
    system = SystemSetup()
    dev = FileSystem.fromDict(x)
    #recode, stdout, stderr = system.exec_chroot(*dev.make_script(), chroot=False)
    try:
        dev.createall(system)
        dev.mountall(system)
    except ExecutionError as e:
        print('stdout', e.stdout)
        print('stderr', e.stderr)

def testfromdict():
    x = {
            'hostname':'localhost',
            'keymap': 'us',
            'consolefont':'CP437',
            'network': 'dhcp',
            'time':{'zone': 'America/New_York',
                    'ntp' : True,
                    },
            'locale':'en_US.UTF-8', #A string or a list of locales
            'mount': '/mnt',
            'chrootcmd':'arch-chroot',
            'getmirrors':True,
            'booter':{'name':'systemd-boot',
                     'device':'/dev/sda',
                     'bootdir':'/boot'
                    },
        }
    system = SystemSetup(x)



if __name__ == '__main__':
    #tester()
    testfromdict()
