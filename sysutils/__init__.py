#!/usr/bin/python3
import pprint
from os.path import isfile
import json
import subprocess
import shlex
import sys

from . import pacmansetup
from . import filesystem
from . import locales
from . import timezones
from . import networking
from . import inits
from .booters import booter
from .helpers import debug, happy, ExecutionError
from .diskutils import FileSystem
from sysinfo import disks

class SystemSetup:
    def __init__(self, config=''):
        self.loadconfig(config)
        self.fs = FileSystem.fromDict(config)

    def getconfig(self):
        return self.__class__.defaultconfig()

    def loadconfig(self, config=''):
        if '' == config or config is None:
            config = self.__class__.defaultconfig()
        elif isinstance(config, dict):
            pass
        elif isfile(config):
            config = json.loads(open(config).read())
        else:
            raise ValueError('config must either be a json file an empty string or a dictionary')
        self.mnt = config['mount']
        self.chrootcmd = config['chrootcmd']
        self.hostname = config['hostname']
        self.config = config

    def popenobj(self, *args, chroot=True, stdout=subprocess.PIPE, shell=False):
        usercmd = list(args)
        if 1 == len(args):
            usercmd = shlex.split(args[0])
        elif not len(args):
            raise ValueError('No command to execute')
        if chroot:
            usercmd = self.chroot_list() + usercmd
        return subprocess.Popen(usercmd, stdout=stdout, stderr=subprocess.PIPE, shell=shell)

    def exec_chroot(self, *args, chroot=True, stdout=subprocess.PIPE, shell=False):
        debug('Running Command : ', *args)
        pop = self.popenobj(*args, chroot=chroot, stdout=stdout, shell=shell)
        stdout, stderr = pop.communicate()
        return (pop.returncode, stdout, stderr)

    def chroot_list(self, shell='/bin/bash'):
        return [ self.chrootcmd,
                 self.mnt ]

    def getroot(self):
        return self.mnt

    def getpath(self, target):
        if not target.startswith('/'):
            target = '/'+target
        return self.getroot() + target

    def runningconfig(self):
        cfg = self.__class__.defaultconfig()
        cfg['mount'] = self.mnt
        cfg['chrootcmd'] = self.chrootcmd
        cfg['hostname'] = self.hostname
        return cfg

    @property
    def root_part(self):
        return disks.find_mount(self.mnt)

    def install_package(self, package, chroot=True):
        pass

    def keys(self, key):
        return self.config.keys()

    def __getitem__(self, key):
        return self.config[key]

    @staticmethod
    def defaultconfig():
        return {
            'hostname':'localhost',
            'keymap': 'us',
            'consolefont':'CP437',
            'network': 'dhcp',
            'time':{'zone': 'America/New_York',
                    'ntp' : True,
                    },
            'locale':'en_US.UTF-8', #A string or a list of locales
            'mount': '/mnt',
            'chrootcmd':'arch-chroot',
            'getmirrors':True,
            'booter':{'name':'systemd-boot',
                     'device':'/dev/sda',
                     'bootdir':'/boot'
                    },
            'disks' : [{
        "device": "sda",    #Required
        "label" : "gpt",    #Required for partitioning
        "partition": [ {"fs":"fat32",    #Required
                        "size" : "100MiB",   #Required
                        "name" : "BOOT",  #Defaults to none
                        "flag" : "boot",  #Defaults to none
                        "type" : "primary",  #Defaults to primary
                        "mount" : "/boot", #Required
                        },
                        {"fs":"ext3",
                        "size" : "-1s",
                        "name" : "ROOT",
                        "type" : "primary",
                        "mount" : "/",
                        }
                      ]
                }],
        }


class ArchInstall(SystemSetup):
    def __init__(self, config=''):
        super().__init__(config)

    def setup_disks(self):
        if self.fs is None:
            return False
        self.fs.createall(self)
        self.fs.mountall(self)
        return True

    def pacmans(self):
        return pacmansetup.pacstrap(self)

    def genfstab(self):
        return filesystem.genfstab(self, fstabfile=self.getpath(filesystem.FSTAB_FILE))

    def timezones(self):
        return timezones.setzone(self['time']['zone'], self.getpath(timezones.LOCALTIME_FILE), self.getpath(timezones.ZONE_DIR))

    def locales(self):
        locales.localegen(self['locale'], self.getpath(locales.LOCALE_GEN))
        return locales.runlocalegen(self)

    def keymap(self):
        locales.setkeymap(self['keymap'], self.getpath(locales.KEYMAP_FILE))

    def sethostname(self):
        if self['hostname'] != 'localhost':
            return networking.SetHostname(self['hostname'], self.getpath(networking.HOSTNAME_FILE), self.getpath(networking.HOSTS_FILE))
        return None

    def mkinitcpio(self):
        inits.run_mkinitcpio(self)

    def booter(self):
        return booter(self, self['booter']['bootdir'])

    def install_system(self):
        self.setup_disks()
        self.pacmans()
        self.genfstab()
        try:
            self.timezones()
        except KeyError:
            pass
        try:
            self.locales()
        except KeyError:
            pass
        try:
            self.sethostname()
        except KeyError:
            pass
        self.mkinitcpio()
        self.booter()

    @classmethod
    def install(cls, configfile):
        system = cls(configfile)
        system.install_system()
        return system

def InstallSystem(configfile):
    system = SystemSetup(configfile)
    try:
        fs = FileSystem.fromDict(configfile)
        fs.createall(system)
        fs.mountall(system)
    except ExecutionError as e:
        print(e)
        print(e.stdout)
        print(e.stderr)
        return 1
    try:
        outs = pacmansetup.pacstrap(system)
        happy('Pacstrap done')
    except ExecutionError as e:
        print(e)
        print(e.stdout)
        print(e.stderr)
        return 1
    filesystem.genfstab(system, fstabfile=system.getpath(filesystem.FSTAB_FILE))
    try:
        timezones.setzone(system['time']['zone'], system.getpath(timezones.LOCALTIME_FILE), system.getpath(timezones.ZONE_DIR))
    except KeyError: #Just ignore and don't set a timezone
        pass
    try:
        locales.localegen(system['locale'], system.getpath(locales.LOCALE_GEN))
        locales.runlocalegen(system)
    except KeyError: #Don't set a locale
        pass
    try:
        locales.setkeymap(system['keymap'], system.getpath(locales.KEYMAP_FILE))
    except KeyError:
        pass
    try:
        if system['hostname'] != 'localhost':
            networking(system['hostname'], system.getpath(networking.HOSTNAME_FILE), system.getpath(networking.HOSTS_FILE))
    except KeyError:
        pass
    inits.run_mkinitcpio(system)
    booter(system, system.config['booter']['bootdir'])


if __name__ == '__main__':
    x = ArchInstall.install(SystemSetup.defaultconfig())
    pprint.PrettyPrinter().pprint(x.getconfig())
